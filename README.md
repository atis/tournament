
Coding task "Social tournament service"
=======================================

Run it
------

Make sure you have [Docker Compose](https://docs.docker.com/compose/install/)
installed.

This should get the containers running and web API listening on port `4123`:

```bash
git clone https://bitbucket.org/atis/tournament.git
cd tournament
sudo docker-compose pull
sudo docker-compose up
```

docker-compose.yml
------------------

Below are the contents of the `docker-compose.yml` file. The exposed web service
port `4123` can be changed if necessary.

```yaml
version: "3"
services:
  db:
    build: ./db
    image: atis/tournament_db
    restart: always
    environment:
      POSTGRES_PASSWORD: richard_feynman_played_the_bongos
    volumes:
      - "pgdata:/var/lib/postgresql/data"

  web:
    build: ./webapp
    image: atis/tournament_web
    restart: unless-stopped
    ports:
      - "4123:80"

volumes:
  pgdata:
```
