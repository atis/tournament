#!/bin/bash
set -e

# create tournament database and webapp user
psql postgres postgres <<-EOSQL
	CREATE DATABASE tournament;
	CREATE USER webapp WITH PASSWORD 'tournament_fun_times';
	GRANT ALL PRIVILEGES ON DATABASE tournament TO webapp;
EOSQL

# create tournament tables
psql tournament webapp <<-EOSQL

	CREATE TABLE players (
		player_id varchar(200) PRIMARY KEY CHECK (char_length(player_id) > 0),
		points bigint NOT NULL CONSTRAINT non_negative_balance CHECK (points >= 0)
	);

	CREATE TABLE tournaments (
		tournament_id varchar(200) PRIMARY KEY CHECK (char_length(tournament_id) > 0),
		deposit bigint NOT NULL CONSTRAINT non_negative_deposit CHECK (deposit >= 0),
		finished boolean NOT NULL DEFAULT FALSE
	);

	-- players having joined a tournament
	CREATE TABLE joins (
		player_id varchar(200) NOT NULL REFERENCES players ON DELETE CASCADE,
		tournament_id varchar(200) NOT NULL REFERENCES tournaments ON DELETE CASCADE,
		CONSTRAINT unique_join UNIQUE (player_id, tournament_id)
	);

	-- players backing other players in a tournament
	CREATE TABLE backers (
		backer_id varchar(200) NOT NULL REFERENCES players ON DELETE CASCADE,
		player_id varchar(200) NOT NULL CONSTRAINT no_self_backing CHECK (player_id != backer_id),
		tournament_id varchar(200) NOT NULL,
		CONSTRAINT join_backing FOREIGN KEY (player_id, tournament_id) REFERENCES joins (player_id, tournament_id) ON DELETE CASCADE,
		CONSTRAINT unique_backing UNIQUE (backer_id, player_id, tournament_id)
	);
EOSQL
