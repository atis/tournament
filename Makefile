
# build standalone webapp image (outside of docker-compose, just for tests)
.PHONY: build-webapp
build-webapp:
	sudo docker build -t webapp ./webapp

# Run tournament_web container with mounted source dir and interactive bash for
# development. Note that we attach to the tournament network. The network-alias
# is unnecessary but is what is created by default by docker-compose so we mimic
# it here just for reference.
.PHONY: mount-webapp
mount-webapp:
	sudo docker run --rm -it \
		-p 4123:80 \
		--mount type=bind,source=`pwd`/webapp,target=/go/src/webapp \
		--network tournament_default \
		--network-alias web \
		atis/tournament_web:latest bash

# use docker-compose to launch all containers
.PHONY: compose-up
compose-up:
	sudo docker-compose up

# use docker-compose to rebuild
.PHONY: compose-build
compose-build:
	sudo docker-compose build

# use docker-compose to push all images to their respective repos
.PHONY: compose-push
compose-push:
	sudo docker-compose push

# attach interactive bash session to a running db container (for exploration)
.PHONY: bash-db
bash-db:
	sudo docker exec -it tournament_db_1 bash

# attach interactive bash session to a running web container (for exploration)
.PHONY: bash-web
bash-web:
	sudo docker exec -it tournament_web_1 bash

# stop and remove all containers from system
.PHONY: remove-containers
remove-containers:
	sudo docker stop `sudo docker ps -q`
	sudo docker container rm `sudo docker ps -aq`

# remove all volumes from system
.PHONY: remove-volumes
remove-volumes:
	sudo docker volume rm `sudo docker volume ls -q`

# remove