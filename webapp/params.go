package main

import (
	"fmt"
	"github.com/pkg/errors"
	"math"
	"net/url"
	"strconv"
	"strings"
)

// numErrorToText assumes the given error is of type *strconv.NumError and
// returns a string describing it.
func numErrorToText(err error) string {
	numErr, ok := err.(*strconv.NumError)
	switch {
	case !ok:
		return "unexpected error"
	case numErr.Err == strconv.ErrRange:
		return "value out of range"
	case numErr.Err == strconv.ErrSyntax:
		return "invalid number syntax"
	default:
		return ""
	}
}

// parseNonNegativePoints converts a string representation of non-negative bonus
// points into int64.
func parseNonNegativePoints(s string) (int64, error) {
	// split by dot in case we have a fractional value
	split := strings.Split(s, ".")
	if len(split) > 2 {
		return -1, errors.New("too many dots")
	}

	// parse and validate the "whole" number part
	whole, err := strconv.ParseInt(split[0], 10, 64)
	if err != nil {
		return -1, errors.New(numErrorToText(err))
	}
	if whole < 0 {
		return -1, errors.New("negative numbers not allowed")
	}

	// parse and validate the fractional number part (if present)
	fraction := int64(0)
	if len(split) > 1 {
		fracStr := split[1]
		if len(fracStr) > 2 {
			return -1, errors.New("too many fractional digits/characters")
		}

		// right-pad with zeros so that ".6" is interpreted correctly as ".60"
		if len(fracStr) < 2 {
			fracStr = fracStr + strings.Repeat("0", 2-len(fracStr))
		}

		var err error
		fraction, err = strconv.ParseInt(fracStr, 10, 0)
		if err != nil {
			return -1, errors.New(numErrorToText(err))
		}
		if fraction < 0 {
			return -1, errors.New("negative fractional part")
		}
	}

	// check if we fit within max integer range
	if whole > (math.MaxInt64-fraction)/100 {
		return -1, errors.New("value overflow")
	}

	return whole*100 + fraction, nil
}

// getNonNegativePointsParam extracts a bonus point query parameter from the
// parsed URL query map. It returns -1 and the accompanying error message in
// case of something unexpected, or the number and and empty string in case of
// success.
//
// Note that values may have fractional parts (e.g. "100.23") but no more than
// two digits are allowed after the decimal point since we're simulating
// something akin to money here. The resulting value is converted to an integer
// by multiplying it by 100 (so "100.23" would result in 10023).
func getNonNegativePointsParam(query url.Values, paramName string) (int64, string) {
	// must have exactly one points param
	paramList := query[paramName]
	if len(paramList) != 1 {
		return -1, fmt.Sprintf("Must have exactly one '%s' query parameter", paramName)
	}

	points, err := parseNonNegativePoints(paramList[0])
	if err != nil {
		return -1, fmt.Sprintf("Unexpected '%s' query parameter (%s)", paramName, err.Error())
	}

	return points, ""
}

// getIdParam extracts the named query parameter (which represents and
// identifier) from the parsed URL query map. It returns an empty string in case
// something is not as it should be and an accompanying error message. The id
// string and an empty error message is returned in case of success.
func getIdParam(query url.Values, paramName string) (string, string) {
	// must have exactly one id param
	paramList := query[paramName]
	if len(paramList) != 1 {
		return "", fmt.Sprintf("Must have exactly one '%s' query parameter", paramName)
	}

	// ensure non-empty id
	id := paramList[0]
	if len(id) == 0 {
		return "", fmt.Sprintf("Empty '%s' query parameter", paramName)
	}

	return id, ""
}

// getMultiIdParams is similar to getIdParam above but returns multiple IDs
// instead. The IDs are checked for non-emptiness and uniqueness.
func getMultiIdParams(query url.Values, paramName string) ([]string, string) {
	paramList := query[paramName]
	idSet := map[string]bool{}
	result := make([]string, len(paramList))
	for i, param := range paramList {
		if len(param) == 0 {
			return nil, fmt.Sprintf("Empty '%s' query parameter value", paramName)
		}
		if idSet[param] {
			return nil, fmt.Sprintf("Duplicate '%s' query parameter value '%s'", paramName, param)
		}
		result[i] = param
		idSet[param] = true
	}
	return result, ""
}
