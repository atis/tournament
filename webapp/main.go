package main

import (
	"database/sql"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	log "github.com/sirupsen/logrus"
	"net/http"
	"time"
)

// database handle used globally
var db *sql.DB

func main() {
	log.Info("tournament webapp start")

	// use logrus for Logger middleware
	middleware.DefaultLogger = middleware.RequestLogger(&middleware.DefaultLogFormatter{
		Logger: log.StandardLogger(),
	})

	// prepare database handle; note that this doesn't actually connect to the
	// DB yet, just validates the args
	var err error
	db, err = sql.Open(
		"postgres",
		"host=db user=webapp dbname=tournament password=tournament_fun_times sslmode=disable",
	)
	if err != nil {
		log.WithError(err).Fatal("db handle prepare error")
	}

	r := chi.NewRouter()

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.Timeout(10 * time.Second))

	r.Get("/fund", handleFundPlayer)
	r.Get("/take", handleTakePlayer)
	r.Get("/announceTournament", handleAnnounceTournament)
	r.Get("/joinTournament", handleJoinTournament)
	r.Post("/resultTournament", handleTournamentResult)
	r.Get("/balance", handlePlayerBalance)
	r.Get("/reset", handleDbReset)
	r.NotFound(func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "Unknown API endpoint", 400)
	})

	log.WithError(http.ListenAndServe(":80", r)).Fatal("webserver error")
}
