package main

import (
	"testing"
)

func TestParseNonNegativePoints(t *testing.T) {
	v, err := parseNonNegativePoints("0")
	if v != 0 || err != nil {
		t.Error(v, err)
	}

	v, err = parseNonNegativePoints("100.23")
	if v != 10023 || err != nil {
		t.Error(v, err)
	}

	v, err = parseNonNegativePoints("0003.02")
	if v != 302 || err != nil {
		t.Error(v, err)
	}

	v, err = parseNonNegativePoints("3.20")
	if v != 320 || err != nil {
		t.Error(v, err)
	}

	v, err = parseNonNegativePoints("3.")
	if v != 300 || err != nil {
		t.Error(v, err)
	}

	v, err = parseNonNegativePoints("3.200")
	if v != -1 || err == nil {
		t.Error(v, err)
	}

	v, err = parseNonNegativePoints("-1")
	if v != -1 || err == nil {
		t.Error(v, err)
	}
}
