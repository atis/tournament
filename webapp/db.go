package main

import (
	"database/sql"
	"fmt"
	"github.com/lib/pq"
	"github.com/pkg/errors"
)

// NOTE: tx-prefixed functions run as part of a larger transaction and are
// called from the db-prefixed functions. db-prefix functions either create a
// transaction explicitly or run a single atomic query within an implicit
// transaction. tx-prefixed functions are internal to this file and are not
// supposed to be invoked by outside callers.

// dbFundPlayer creates a player with the given ID and number of points. If the
// identified player already exists, the points will be atomically added to its
// balance.
func dbFundPlayer(playerId string, points int64) error {
	err := db.QueryRow(`
		INSERT INTO players (player_id, points) VALUES ($1, $2)
		ON CONFLICT (player_id) DO UPDATE
			SET points = Players.points + EXCLUDED.points`, playerId, points).Scan()
	if err == sql.ErrNoRows {
		return nil
	}
	return err
}

// dbTakePlayer subtracts the number of points from the player's balance. In
// case the player does not exist, or if the balance were to drop below zero, an
// error is produced.
func dbTakePlayer(playerId string, points int64) error {
	// to avoid creating a transaction here, we use the postgres WITH clause to
	// get the number of updated rows (either zero or one)
	var count int
	err := db.QueryRow(`
		WITH rows AS (
			UPDATE players SET points = points - $2
			WHERE player_id = $1
			RETURNING 1
		)
		SELECT count(*) FROM rows`, playerId, points).Scan(&count)
	if err != nil {
		return err
	}
	if count == 0 {
		return errors.New(fmt.Sprintf("Player '%s' does not exist", playerId))
	}
	return nil
}

func dbAnnounceTournament(tournamentId string, deposit int64) error {
	err := db.QueryRow(`
		INSERT INTO tournaments (tournament_id, deposit)
		VALUES ($1, $2)`, tournamentId, deposit).Scan()
	if err == sql.ErrNoRows {
		return nil
	}

	// report that given tournament id already exists by checking the pq error
	// Constraint field
	if pqerr, ok := err.(*pq.Error); ok {
		if pqerr.Constraint == "tournaments_pkey" {
			return errors.New(fmt.Sprintf("Tournament '%s' already exists!", tournamentId))
		}
	}
	return err
}

// txChargePlayer will subtract from players's balance. An error is returned in
// case the player does not exist or their balance goes below zero (among other
// possible failures).
func txChargePlayer(tx *sql.Tx, playerId string, points int64) error {
	res, err := tx.Exec(
		`UPDATE players SET points = points - $2
			WHERE player_id = $1`, playerId, points)
	if err != nil {
		if pqerr, ok := err.(*pq.Error); ok {
			if pqerr.Constraint == "non_negative_balance" {
				return errors.New(fmt.Sprintf("Player '%s' does not have enough points!", playerId))
			}
		}
		return err
	}

	// check if anything was updated
	rows, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if rows == 0 {
		return errors.New(fmt.Sprintf("Player '%s' does not exist", playerId))
	}
	return nil
}

// txGetTournamentDeposit returns the required tournament deposit fee. An error
// is returned in case the tournament ID does not exist or the tournament is
// finished (amongst other possible errors).
func txGetTournamentDeposit(tx *sql.Tx, tournamentId string) (int64, error) {
	var deposit int64
	var finished bool
	err := tx.QueryRow(`
		SELECT deposit, finished
		FROM tournaments
		WHERE tournament_id = $1`, tournamentId).Scan(&deposit, &finished)
	if err != nil {
		if err == sql.ErrNoRows {
			return 0, errors.New(fmt.Sprintf("Tournament '%s' does not exist", tournamentId))
		}
		return 0, err
	}
	if finished {
		return 0, errors.New(fmt.Sprintf("Tournament '%s' is finished", tournamentId))
	}
	return deposit, nil
}

// txFinishTournament sets the "finished" flag for the provided tournament ID.
// An error is returned in case the tournament does not exist or is already
// finished or if any other failure is encountered.
func txFinishTournament(tx *sql.Tx, tournamentId string) error {
	var finished bool
	err := tx.QueryRow(`
		SELECT finished
		FROM tournaments
		WHERE tournament_id = $1`, tournamentId).Scan(&finished)
	if err != nil {
		if err == sql.ErrNoRows {
			return errors.New(fmt.Sprintf("Tournament '%s' does not exist", tournamentId))
		}
		return err
	}
	if finished {
		return errors.New(fmt.Sprintf("Tournament '%s' is already finished", tournamentId))
	}

	_, err = tx.Exec(
		`UPDATE tournaments SET finished = TRUE
			WHERE tournament_id = $1`, tournamentId)
	return err
}

// txCreateJoinsEntry creates the "joins" table entry for player/tournament pair
// so we know that player has already joined. If the player has already joined,
// then this will fail the transaction.
func txCreateJoinsEntry(tx *sql.Tx, tournamentId, playerId string) error {
	_, err := tx.Exec(`
		INSERT INTO joins (tournament_id, player_id)
		VALUES ($1, $2)`, tournamentId, playerId)
	if err != nil {
		if pqerr, ok := err.(*pq.Error); ok && pqerr.Constraint == "unique_join" {
			return errors.New(
				fmt.Sprintf(
					"Player '%s' has already joined tournament '%s'!",
					playerId,
					tournamentId,
				),
			)
		}
		return err
	}
	return nil
}

// txEnsureTournamentPlayer returns an error in case the player did not
// participate in the tournament.
func txEnsureTournamentPlayer(tx *sql.Tx, tournamentId, playerId string) error {
	err := tx.QueryRow(`
		SELECT FROM joins
		WHERE tournament_id = $1 AND player_id = $2`, tournamentId, playerId).Scan()
	if err != nil {
		if err == sql.ErrNoRows {
			return errors.New(fmt.Sprintf("Player '%s' did not participate in tournament '%s'", playerId, tournamentId))
		}
		return err
	}
	return nil
}

func dbGetPlayerBalance(playerId string) (int64, error) {
	var points int64
	err := db.QueryRow(`
		SELECT points FROM players
		WHERE player_id = $1`, playerId).Scan(&points)
	if err != nil {
		if err == sql.ErrNoRows {
			return 0, errors.New(fmt.Sprintf("Player '%s' does not exist", playerId))
		}
		return 0, err
	}
	return points, nil
}

// txGetBackers returns a list of player's backers in a tournament.
func txGetBackers(tx *sql.Tx, tournamentId, playerId string) ([]string, error) {
	rows, err := tx.Query(`
		SELECT backer_id FROM backers
		WHERE tournament_id = $1 AND player_id = $2`, tournamentId, playerId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	backers := make([]string, 0)
	for rows.Next() {
		var backerId string
		if err = rows.Scan(&backerId); err != nil {
			return nil, err
		}
		backers = append(backers, backerId)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return backers, nil
}

// txRegisterBacker creates an entry in the "backers" table. The tournament-id &
// player-id & backer-id combo is supposed to be unique so an error is returned
// if that gets violated. Also backers are only supposed to back players already
// enrolled in a tournament, and not back themselves!
func txRegisterBacker(tx *sql.Tx, tournamentId, playerId, backerId string) error {
	_, err := tx.Exec(`
		INSERT INTO backers (tournament_id, player_id, backer_id)
		VALUES($1, $2, $3)`, tournamentId, playerId, backerId)
	if err != nil {
		if pqerr, ok := err.(*pq.Error); ok {
			if pqerr.Constraint == "unique_backing" {
				return errors.New(
					fmt.Sprintf(
						"Player '%s' already backing player '%s' in tournament '%s'!",
						backerId,
						playerId,
						tournamentId,
					),
				)
			}
			if pqerr.Constraint == "join_backing" {
				return errors.New(
					fmt.Sprintf(
						"Player '%s' is backing player '%s' which does not play in tournament '%s'!",
						backerId,
						playerId,
						tournamentId,
					),
				)
			}
			if pqerr.Constraint == "no_self_backing" {
				return errors.New(
					fmt.Sprintf(
						"Player '%s' cannot back herself in tournament '%s'!",
						backerId,
						tournamentId,
					),
				)
			}
		}
		return err
	}
	return nil
}

// txFundPlayer adds points to player's balance. Player existence is assumed and
// not checked.
func txFundPlayer(tx *sql.Tx, playerId string, points int64) error {
	_, err := tx.Exec(`
		UPDATE players SET points = points + $2
		WHERE player_id = $1`, playerId, points)
	return err
}

// dbJoinTournament runs the complete player-joins-a-tournament transaction.
// Player and backer balances are updated, and a "joins" entry is created.
func dbJoinTournament(tournamentId, playerId string, backers []string) error {
	tx, err := db.Begin()
	if err != nil {
		return err
	}

	// get tournament deposit fee so we know how much each player should be
	// charged
	deposit, err := txGetTournamentDeposit(tx, tournamentId)
	if err != nil {
		tx.Rollback()
		return err
	}

	// Split up the deposit to know how much each player has to pay up. Note
	// that depending on the deposit amount, it may not divide up evenly amongst
	// backers and the player. If so, we make the player pay a bit more so that
	// all backers pay an equal share.
	nBackers := int64(len(backers))
	backerShare := deposit / (nBackers + 1)
	playerShare := deposit - backerShare*nBackers

	// charge player
	if err = txChargePlayer(tx, playerId, playerShare); err != nil {
		tx.Rollback()
		return err
	}

	// make player join the tournament; ensure that they already haven't done so
	if err = txCreateJoinsEntry(tx, tournamentId, playerId); err != nil {
		tx.Rollback()
		return err
	}

	for _, backerId := range backers {
		// charge backer
		if err = txChargePlayer(tx, backerId, backerShare); err != nil {
			tx.Rollback()
			return err
		}

		// register backer backing player in this tournament
		if err = txRegisterBacker(tx, tournamentId, playerId, backerId); err != nil {
			tx.Rollback()
			return err
		}
	}

	tx.Commit()
	return nil
}

func dbFinishTournament(tr *TournamentResult) error {
	tx, err := db.Begin()
	if err != nil {
		return err
	}

	// ensure that tournament exists and set finished=true for it
	err = txFinishTournament(tx, tr.TournamentId)
	if err != nil {
		tx.Rollback()
		return err
	}

	// process winning players
	for _, winner := range tr.Winners {
		// player must have participated
		err = txEnsureTournamentPlayer(tx, tr.TournamentId, winner.PlayerId)
		if err != nil {
			tx.Rollback()
			return err
		}

		// get all player's backers so we can credit them
		backers, err := txGetBackers(tx, tr.TournamentId, winner.PlayerId)
		if err != nil {
			tx.Rollback()
			return err
		}

		// split up prize money; if cannot be divided evenly, then most goes to
		// player (same logic as with the deposit)
		nBackers := int64(len(backers))
		backerShare := winner.prize / (nBackers + 1)
		playerShare := winner.prize - backerShare*nBackers

		// fund player
		if err = txFundPlayer(tx, winner.PlayerId, playerShare); err != nil {
			tx.Rollback()
			return err
		}

		// fund backers
		for _, backerId := range backers {
			if err = txFundPlayer(tx, backerId, backerShare); err != nil {
				tx.Rollback()
				return err
			}
		}
	}

	tx.Commit()
	return nil
}

// dbReset deletes all data from all tables.
func dbReset() error {
	tx, err := db.Begin()
	if err != nil {
		return err
	}

	// Only delete from players and tournaments: the other tables are cleared
	// automatically because of foreign key cascades.
	if _, err = tx.Exec(`DELETE FROM players`); err != nil {
		tx.Rollback()
		return err
	}
	if _, err = tx.Exec(`DELETE FROM tournaments`); err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	return nil
}
