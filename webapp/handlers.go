package main

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"net/http"
	"strconv"
	"strings"
)

func handleFundPlayer(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	playerId, errmsg := getIdParam(query, "playerId")
	if len(errmsg) > 0 {
		http.Error(w, errmsg, 400)
		return
	}

	points, errmsg := getNonNegativePointsParam(query, "points")
	if len(errmsg) > 0 {
		http.Error(w, errmsg, 400)
		return
	}

	err := dbFundPlayer(playerId, points)
	if err != nil {
		log.WithError(err).Error("db fund player points error")
		http.Error(w, err.Error(), 500)
		return
	}

	w.Write([]byte(fmt.Sprintf("Player '%s' funded!", playerId)))
}

func handleTakePlayer(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	playerId, errmsg := getIdParam(query, "playerId")
	if len(errmsg) > 0 {
		http.Error(w, errmsg, 400)
		return
	}

	points, errmsg := getNonNegativePointsParam(query, "points")
	if len(errmsg) > 0 {
		http.Error(w, errmsg, 400)
		return
	}

	err := dbTakePlayer(playerId, points)
	if err != nil {
		log.WithError(err).Error("db take payer points error")
		http.Error(w, err.Error(), 500)
		return
	}

	w.Write([]byte(fmt.Sprintf("Player '%s' charged!", playerId)))
}

func handleAnnounceTournament(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	tournamentId, errmsg := getIdParam(query, "tournamentId")
	if len(errmsg) > 0 {
		http.Error(w, errmsg, 400)
		return
	}

	deposit, errmsg := getNonNegativePointsParam(query, "deposit")
	if len(errmsg) > 0 {
		http.Error(w, errmsg, 400)
		return
	}

	err := dbAnnounceTournament(tournamentId, deposit)
	if err != nil {
		log.WithError(err).Error("db announce tournament error")
		http.Error(w, err.Error(), 500)
		return
	}

	w.Write([]byte(fmt.Sprintf("Tournament '%s' announced!", tournamentId)))
}

func handleJoinTournament(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	tournamentId, errmsg := getIdParam(query, "tournamentId")
	if len(errmsg) > 0 {
		http.Error(w, errmsg, 400)
		return
	}

	playerId, errmsg := getIdParam(query, "playerId")
	if len(errmsg) > 0 {
		http.Error(w, errmsg, 400)
		return
	}

	backerIds, errmsg := getMultiIdParams(query, "backerId")
	if backerIds == nil {
		http.Error(w, errmsg, 400)
		return
	}

	// basic check for player not backing themselves
	for _, backer := range backerIds {
		if playerId == backer {
			http.Error(w, fmt.Sprintf("Player '%s' cannot back herself!", playerId), 400)
			return
		}
	}

	err := dbJoinTournament(tournamentId, playerId, backerIds)
	if err != nil {
		log.WithError(err).Error("db join tournament error")
		http.Error(w, err.Error(), 500)
		return
	}

	w.Write([]byte(fmt.Sprintf("Player '%s' joined tournament '%s'!", playerId, tournamentId)))
}

type Winner struct {
	PlayerId    string      `json:"playerId"`
	PrizeString json.Number `json:"prize"`
	prize       int64
}

type TournamentResult struct {
	TournamentId string   `json:"tournamentId"`
	Winners      []Winner `json:"winners"`
}

func handleTournamentResult(w http.ResponseWriter, r *http.Request) {
	var tr TournamentResult
	err := json.NewDecoder(r.Body).Decode(&tr)
	if err != nil {
		log.WithError(err).Error("tournament result json parse error")
		http.Error(w, err.Error(), 400)
		return
	}

	// for checking duplicate winners
	winnerSet := map[string]bool{}

	// convert prize strings to int64
	for i := range tr.Winners {
		// parse/validate prize amount
		winner := &tr.Winners[i]
		winner.prize, err = parseNonNegativePoints(string(winner.PrizeString))
		if err != nil {
			http.Error(w, fmt.Sprintf("Invalid prize points format (%s)", err.Error()), 400)
			return
		}

		// check dupe
		if winnerSet[winner.PlayerId] {
			http.Error(w, fmt.Sprintf("Duplicate winner '%s'", winner.PlayerId), 400)
			return
		}
		winnerSet[winner.PlayerId] = true
	}

	if err = dbFinishTournament(&tr); err != nil {
		log.WithError(err).Error("db finish tournament error")
		http.Error(w, err.Error(), 500)
		return
	}

	w.Write([]byte(fmt.Sprintf("Tournament '%s' finished!\n", tr.TournamentId)))
}

type balanceResponse struct {
	PlayerId string      `json:"playerId"`
	Balance  json.Number `json:"balance"`
}

func handlePlayerBalance(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	playerId, errmsg := getIdParam(query, "playerId")
	if len(errmsg) > 0 {
		http.Error(w, errmsg, 400)
		return
	}

	points, err := dbGetPlayerBalance(playerId)
	if err != nil {
		log.WithError(err).Error("db get balance error")
		http.Error(w, err.Error(), 500)
		return
	}

	// prepare balance response taking care to represent the number correctly
	remainder := points % 100
	remStr := strconv.FormatInt(remainder, 10)
	br := balanceResponse{
		PlayerId: playerId,
		Balance: json.Number(
			fmt.Sprintf("%d.%s%s", points/100, strings.Repeat("0", 2-len(remStr)), remStr),
		),
	}

	resp, err := json.Marshal(br)
	if err != nil {
		log.WithError(err).Error("balance response json error")
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(resp)
}

func handleDbReset(w http.ResponseWriter, r *http.Request) {
	if err := dbReset(); err != nil {
		log.WithError(err).Error("balance response json error")
		http.Error(w, err.Error(), 500)
		return
	}
	w.Write([]byte("Database reset!"))
}
